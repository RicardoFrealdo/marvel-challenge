//
//  FavoritesViewController.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/2/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit
import CoreData

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    let reuseIdentifier = "favoriteTableViewCell"
    @IBOutlet weak var uiTableView: UITableView?
    
    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FavoritesViewController.willReload), name: "shouldReload", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.uiTableView?.reloadData()
    }
    
    // Mark: - Private Methods
    
    func willReload(){
        self.uiTableView?.reloadData()
    }
    
    // MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserSaves.sharedInstance.savedHeroes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as! FavoriteTableViewCell
        let hero = UserSaves.sharedInstance.savedHeroes[indexPath.row]
        
        cell.uiHeroName!.text = hero.valueForKey("name") as? String
        
        return cell
    }
}
