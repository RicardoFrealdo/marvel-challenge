//
//  EventTableViewCell.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/2/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var uiEventThumb: UIImageView?
    @IBOutlet weak var uiEventName: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
