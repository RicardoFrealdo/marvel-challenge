//
//  ViewController.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/1/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit
import CoreData

class UserSaves {
    static let sharedInstance = UserSaves()
    var savedHeroes = [NSManagedObject]()
}

class HeroesListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    var heroesList = [Hero]()
    let searchController = UISearchController(searchResultsController: nil)
    var filteredHeroes = [Hero]()
    let uiView = UIView()
    let refreshControl = UIRefreshControl()

    @IBOutlet weak var uiTableView: UITableView?
    
    let reuseIdentifier = "heroTableViewCell"
    
    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let feedManager = DownloadFeedManager()
        feedManager.downloadHeroesListWithOffset(0)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HeroesListViewController.reloadTableViewWithHeroes(_:)), name: "sendHeroes", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HeroesListViewController.saveHeroName(_:)), name: "sendHeroNameToSave", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HeroesListViewController.deleteHeroName(_:)), name: "sendHeroNameToDelete", object: nil)
        
        self.uiView.frame = CGRectMake(0, 0, self.view.frame.size.width, 80)
        let pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        pagingSpinner.startAnimating()
        pagingSpinner.hidesWhenStopped = true
        pagingSpinner.frame.origin = CGPointMake(uiView.frame.size.width/2, uiView.frame.size.height/2)
        uiView.addSubview(pagingSpinner)
        uiTableView?.tableFooterView = self.uiView
        
        refreshControl.addTarget(self, action: #selector(HeroesListViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.uiTableView?.addSubview(refreshControl) // not required when using UITableViewController
        
        //SearchController parameters
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        uiTableView?.tableHeaderView = self.searchController.searchBar
        
        //CoreData Fetching
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Hero")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            UserSaves.sharedInstance.savedHeroes = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.uiTableView?.reloadData()
    }
    
    
    // MARK: - Private Methods
    
    func reloadTableViewWithHeroes(notification: NSNotification) {
        let myDict = notification.userInfo as! [NSString: AnyObject]
        let auxArray = myDict["heroes"] as! [Hero]
        self.heroesList.appendContentsOf(auxArray)
        self.reorderHeroArray()
        guard let tableView = self.uiTableView else {
            return
        }
        tableView.reloadData()
    }
    
    func reorderHeroArray(){
        for hero in UserSaves.sharedInstance.savedHeroes {
            for (i, heroObject) in self.heroesList.enumerate() {
                if hero.valueForKey("name") as! String == heroObject.name {
                    let aux = self.heroesList.removeAtIndex(i)
                    self.heroesList.insert(aux, atIndex: 0)
                }
            }
        }
    }
    
    func refresh(refreshControl: UIRefreshControl) {
        self.heroesList.sortInPlace() { $0.name < $1.name }
        self.reorderHeroArray()
        self.uiTableView?.reloadData()
        refreshControl.endRefreshing()
    }
    
    // MARK: - CoreData
    
    func saveHeroName(notification: NSNotification) {
        let myDict = notification.userInfo as! [NSString: NSString]
        let name = myDict["name"]
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let entity =  NSEntityDescription.entityForName("Hero",
                                                        inManagedObjectContext:managedContext)
        let hero = NSManagedObject(entity: entity!,
                                     insertIntoManagedObjectContext: managedContext)
        hero.setValue(name, forKey: "name")
        
        do {
            try managedContext.save()
            UserSaves.sharedInstance.savedHeroes.append(hero)
            self.reorderHeroArray()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        self.uiTableView?.reloadData()
    }
    
    func deleteHeroName(notification: NSNotification) {
        let myDict = notification.userInfo as! [NSString: NSString]
        let name = myDict["name"]
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        for (i, hero) in UserSaves.sharedInstance.savedHeroes.enumerate() {
            let nameFromData = hero.valueForKey("name") as! NSString
            if nameFromData == name {
                managedContext.deleteObject(hero)
                UserSaves.sharedInstance.savedHeroes.removeAtIndex(i)
            }
        }
        
        do {
            try managedContext.save()
            self.reorderHeroArray()
        } catch let error as NSError  {
            print("Could not delete \(error), \(error.userInfo)")
        }
        self.uiTableView?.reloadData()
        NSNotificationCenter.defaultCenter().postNotificationName("shouldReload", object: self, userInfo: nil)
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let destinationVC = segue.destinationViewController as? HeroDetailsViewController else {
            print("[ERROR] Couldnt creat VC for HeroDetails")
            return
        }
        destinationVC.selectedHero = sender as? Hero
    }
    
    
    // MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.active && searchController.searchBar.text != "" {
            return self.filteredHeroes.count
        }
        return self.heroesList.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if searchController.active && searchController.searchBar.text != "" {
            performSegueWithIdentifier("goToHeroDetails", sender: self.filteredHeroes[indexPath.item])
        } else {
            performSegueWithIdentifier("goToHeroDetails", sender: self.heroesList[indexPath.item])
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as! HeroTableViewCell
        
        let oneHero : Hero
        if searchController.active && searchController.searchBar.text != "" {
            oneHero = self.filteredHeroes[indexPath.row]
        } else {
            oneHero = self.heroesList[indexPath.row]
        }

        cell.uiHeroThumb?.image = nil
        
        if oneHero.thumbSmall != ""{
            let imageUrl = NSURL(string: oneHero.thumbSmall)
            let imageRequest: NSURLRequest = NSURLRequest(URL: imageUrl!)
            let queue: NSOperationQueue = NSOperationQueue.mainQueue()
            NSURLConnection.sendAsynchronousRequest(imageRequest, queue: queue, completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                if data != nil {
                    let image = UIImage(data: data!)
                    cell.uiHeroThumb?.image = image
                }
            })
        } else {
            cell.uiHeroThumb?.image = UIImage (named: "no-image-avaliable")
        }
        cell.uiHeroName?.text = oneHero.name
        
        var didFavorite : Bool = false
        for hero in UserSaves.sharedInstance.savedHeroes {
            if hero.valueForKey("name") as! String == oneHero.name {
                cell.uiFavoriteButton?.setBackgroundImage(UIImage (named: "favorite_on"), forState: .Normal)
                didFavorite = true
            }
        }
        if didFavorite == false {
            cell.uiFavoriteButton?.setBackgroundImage(UIImage (named: "favorite_off"), forState: .Normal)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == (self.heroesList.count - 1) {
            let feedManager = DownloadFeedManager()
            feedManager.downloadHeroesListWithOffset(self.heroesList.count)
        }
    }
    
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        self.filteredHeroes = self.heroesList.filter { hero in
            return hero.name.lowercaseString.containsString(searchText.lowercaseString)
        }
        uiTableView?.reloadData()
        if searchController.active && searchController.searchBar.text != "" {
            self.uiTableView?.tableFooterView = nil
        } else {
            self.uiTableView?.tableFooterView = self.uiView
        }
    }
}

extension HeroesListViewController: UISearchResultsUpdating {
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
