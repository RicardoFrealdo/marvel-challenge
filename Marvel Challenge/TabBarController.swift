//
//  TabBarController.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/1/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        
        if Reachability.isConnectedToNetwork() == true {
            self.selectedIndex = 0
        } else {
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
            self.selectedIndex = 1
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
