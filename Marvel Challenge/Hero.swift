//
//  Hero.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/1/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit

class Hero: NSObject {

    var id : Int = 0
    var name : String = ""
    var heroDescription: String = ""
    var thumbSmall : String = ""
    var thumbLarge : String = ""
    
    required init(withRepresentation r: NSDictionary){
        guard let idKey = r["id"] as? Int else {
            print("[ERROR] - NO HERO ID")
            return
        }
        self.id = idKey
        //print(idKey)
        guard let nameKey = r["name"] as? String else {
            print("[ERROR] - NO HERO NAME")
            return
        }
        self.name = nameKey
        //print(nameKey)
        guard let descriptionKey = r["description"] as? String else {
            print("[ERROR] - NO HERO DESCRIPTION")
            return
        }
        self.heroDescription = descriptionKey
        //print(descriptionKey)
        
        guard let thumbDic = r["thumbnail"] as? [String: AnyObject],
            let thumbPath = thumbDic["path"] as? String,
            let thumbExtension = thumbDic["extension"] as? String else {
                print("[ERROR] - NO HERO THUMBNAIL")
                return
        }
        //standard_small
        //standard_large
        //landscape_small
        self.thumbSmall = thumbPath+"/landscape_small."+thumbExtension
        self.thumbLarge = thumbPath+"/landscape_large."+thumbExtension
        //print(thumbPath+"/standard_small."+thumbExtension)
    }
    
}
