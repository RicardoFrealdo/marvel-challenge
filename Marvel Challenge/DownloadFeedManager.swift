//
//  DownloadFeedManager.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/1/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit
import Alamofire
import CryptoSwift


class DownloadFeedManager: NSObject{
    
    let publicApiKey : String = "cd1383509831e4d2a8bcc0e99000828a"
    let privateApiKey : String = "76ad7bc61f8b98f3d815624e606d91e2ccfa806b"
    let baseURL : String = "http://gateway.marvel.com/"
    let heroesURL : String = "v1/public/characters"
    var lastTimestamp : String = ""
    var heroID : String = ""
    
    var eventsURL : String {
       return "v1/public/characters/" + self.heroID + "/events"
    }
    
    var timestamp: String {
        return "\(NSDate().timeIntervalSince1970 * 1000)"
    }
    
    var newHash : String {
        return (self.lastTimestamp+self.privateApiKey+self.publicApiKey).md5()
    }
    
    var objectList : NSMutableArray = []
    
    func downloadHeroesListWithOffset(offset: Int){
        //Final URL to make the heroes request
        let urlString : String = self.baseURL + self.heroesURL
        //Saving timestamp to create a hash and send the timestamp used to create it
        self.lastTimestamp = self.timestamp
        
        let parameters = [
            "ts" : self.lastTimestamp,
            "apikey": self.publicApiKey,
            "hash" : newHash,
            "limit" : "25",
            "offset" : "\(offset)"
        ]

        Alamofire.request(.GET, urlString, parameters: parameters, encoding: .URLEncodedInURL)
        
            .responseJSON { response in
                if let _ = response.result.value {
                    //print("JSON: \(jsonString)")
                    guard let data = response.data else {
                        return
                    }
                    self.parseJsonWithData(data, forObjectType: "heroes")
                } else {

                }
            }
    }

    
    func downloadHeroEvents(withID id: Int, offset: Int){
        self.heroID = "\(id)"
        //Final URL to make the events request
        let urlString : String = self.baseURL + self.eventsURL
        //Saving timestamp to create a hash and send the timestamp used to create it
        self.lastTimestamp = self.timestamp
        
        let parameters = [
            "ts" : self.lastTimestamp,
            "apikey": self.publicApiKey,
            "hash" : newHash,
            "limit" : "25",
            "offset" : "\(offset)"
        ]
        
        Alamofire.request(.GET, urlString, parameters: parameters, encoding: .URLEncodedInURL)
            
            .responseJSON { response in
                if let _ = response.result.value {
                    //print("JSON: \(jsonString)")
                    guard let data = response.data else {
                        return
                    }
                    self.parseJsonWithData(data, forObjectType: "events")
                } else {
                    
                }
        }
    }

    //ObjectType can be HEROES ou EVENTS
    func parseJsonWithData(data : NSData, forObjectType type: String){
        
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)

            guard let data = json["data"] as? NSDictionary,
                    let results = data["results"] as? [[String: AnyObject]] else {
                print("[ERROR] Might have reached the end")
                return
            }
            if results.count == 0 {
                if type == "heroes" {
                    NSNotificationCenter.defaultCenter().postNotificationName("heroesLimit", object: self, userInfo: nil)
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName("eventsLimit", object: self, userInfo: nil)
                }
            }
            for result in results {
                if type == "heroes" {
                    let oneHero = Hero(withRepresentation: result)
                    self.objectList.addObject(oneHero)
                } else {
                    let oneEvent = Events(withRepresentation: result)
                    self.objectList.addObject(oneEvent)
                }
            }
            
        } catch {
            print("error serializing JSON: \(error)")
        }
        
        if type == "heroes" {
            let myHeroesDict : [String:NSMutableArray] = ["heroes": self.objectList]
            NSNotificationCenter.defaultCenter().postNotificationName("sendHeroes", object: self, userInfo: myHeroesDict)
        } else {
            let myEventsDict : [String:NSMutableArray] = ["events": self.objectList]
            NSNotificationCenter.defaultCenter().postNotificationName("sendEvents", object: self, userInfo: myEventsDict)
        }
        
    }
    
}

