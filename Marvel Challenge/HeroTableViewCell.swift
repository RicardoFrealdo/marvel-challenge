//
//  HeroTableViewCell.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/1/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit

class HeroTableViewCell: UITableViewCell {

    @IBOutlet weak var uiHeroThumb: UIImageView?
    @IBOutlet weak var uiHeroName: UILabel?
    @IBOutlet weak var uiFavoriteButton: UIButton?
    
    @IBAction func saveAction(sender: AnyObject) {
        
        let myHeroDict : [String:String] = ["name": (self.uiHeroName?.text)!]
        var didDelete : Bool = false
        for hero in UserSaves.sharedInstance.savedHeroes {
            if hero.valueForKey("name") as? String == self.uiHeroName?.text{
                NSNotificationCenter.defaultCenter().postNotificationName("sendHeroNameToDelete", object: self, userInfo: myHeroDict)
                didDelete = true
            }
        }
        if didDelete == false {
             NSNotificationCenter.defaultCenter().postNotificationName("sendHeroNameToSave", object: self, userInfo: myHeroDict)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
