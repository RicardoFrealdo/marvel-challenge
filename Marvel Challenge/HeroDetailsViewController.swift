//
//  HeroDetailsViewController.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/2/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit

class HeroDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var selectedHero = Hero?()
    var heroEvents : NSMutableArray = []
    var eventsReachedLimit : Bool = false
    let reuseIdentifier = "eventTableViewCell"
    
    @IBOutlet weak var uiHeroThumb: UIImageView?
    @IBOutlet weak var uiHeroName: UILabel?
    @IBOutlet weak var uiHeroDescription: UILabel?
    @IBOutlet weak var uiTableView: UITableView?
    @IBOutlet weak var uiSaveButton: UIButton?
    
    
    @IBAction func saveAction(sender: AnyObject) {
        
        let myHeroDict : [String:String] = ["name": (self.uiHeroName?.text)!]
        var didDelete : Bool = false
        for hero in UserSaves.sharedInstance.savedHeroes {
            if hero.valueForKey("name") as? String == self.uiHeroName?.text{
                NSNotificationCenter.defaultCenter().postNotificationName("sendHeroNameToDelete", object: self, userInfo: myHeroDict)
                didDelete = true
                self.uiSaveButton?.setBackgroundImage(UIImage (named: "favorite_off"), forState: .Normal)
            }
        }
        if didDelete == false {
            NSNotificationCenter.defaultCenter().postNotificationName("sendHeroNameToSave", object: self, userInfo: myHeroDict)
            self.uiSaveButton?.setBackgroundImage(UIImage (named: "favorite_on"), forState: .Normal)
        }
        
    }

    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.uiHeroName?.text = self.selectedHero?.name
        
        if self.selectedHero?.thumbLarge != ""{
            let imageUrl = NSURL(string: (self.selectedHero?.thumbLarge)!)
            let imageRequest: NSURLRequest = NSURLRequest(URL: imageUrl!)
            let queue: NSOperationQueue = NSOperationQueue.mainQueue()
            NSURLConnection.sendAsynchronousRequest(imageRequest, queue: queue, completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                if data != nil {
                    let image = UIImage(data: data!)
                    self.uiHeroThumb?.image = image
                }
            })
        } else {
            self.uiHeroThumb?.image = UIImage (named: "no-image-avaliable")
        }
        if self.selectedHero?.heroDescription != "" {
            self.uiHeroDescription?.text = self.selectedHero?.heroDescription
        } else {
            self.uiHeroDescription?.text = "No description."
        }
        
        let feedManager = DownloadFeedManager()
        feedManager.downloadHeroEvents(withID: (self.selectedHero?.id)!, offset: 0)
    
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HeroDetailsViewController.reloadTableViewWithEvents(_:)), name: "sendEvents", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HeroDetailsViewController.changeEventsFlag), name: "eventsLimit", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        var didEnter : Bool = false
        for hero in UserSaves.sharedInstance.savedHeroes {
            if hero.valueForKey("name") as? String == self.uiHeroName?.text{
                self.uiSaveButton?.setBackgroundImage(UIImage (named: "favorite_on"), forState: .Normal)
                didEnter = true
            }
        }
        if didEnter == false {
            self.uiSaveButton?.setBackgroundImage(UIImage (named: "favorite_off"), forState: .Normal)
        }
    }
    
    // MARK: - Private Methods
    func changeEventsFlag(){
        self.eventsReachedLimit = true
    }
    
    func reloadTableViewWithEvents(notification: NSNotification) {
        let myDict = notification.userInfo as! [NSString: AnyObject]
        let auxArray = myDict["events"] as! [AnyObject]
        self.heroEvents.addObjectsFromArray(auxArray)
        guard let tableView = self.uiTableView else {
            return
        }
        tableView.reloadData()
    }
    

    // MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.heroEvents.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.heroEvents.count == 0 {
            return "No Events"
        }
        return "Events"
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as! EventTableViewCell
        let oneEvent = self.heroEvents .objectAtIndex(indexPath.row) as! Events
        
        cell.uiEventThumb?.image = nil
        
        if oneEvent.thumbLarge != ""{
            let imageUrl = NSURL(string: oneEvent.thumbLarge)
            let imageRequest: NSURLRequest = NSURLRequest(URL: imageUrl!)
            let queue: NSOperationQueue = NSOperationQueue.mainQueue()
            NSURLConnection.sendAsynchronousRequest(imageRequest, queue: queue, completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                if data != nil {
                    let image = UIImage(data: data!)
                    cell.uiEventThumb?.image = image
                }
            })
        } else {
            cell.uiEventThumb?.image = UIImage (named: "no-image-avaliable")
        }
        cell.uiEventName?.text = oneEvent.name
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if self.eventsReachedLimit == false {
            if indexPath.row == (self.heroEvents.count - 1) {
                let feedManager = DownloadFeedManager()
                feedManager.downloadHeroEvents(withID: (self.selectedHero?.id)!, offset: self.heroEvents.count)
            }
        }
    }
}
