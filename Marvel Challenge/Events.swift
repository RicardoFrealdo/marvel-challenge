//
//  Events.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/2/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit

class Events: NSObject {

    var id : Int = 0
    var name : String = ""
    var thumbSmall : String = ""
    var thumbLarge : String = ""
    
    required init(withRepresentation r: NSDictionary){
        guard let idKey = r["id"] as? Int else {
            print("[ERROR] - NO EVENT ID")
            return
        }
        self.id = idKey
        //print(idKey)
        guard let nameKey = r["title"] as? String else {
            print("[ERROR] - NO EVENT NAME")
            return
        }
        self.name = nameKey
        //print(nameKey)
        guard let thumbDic = r["thumbnail"] as? [String: AnyObject],
            let thumbPath = thumbDic["path"] as? String,
            let thumbExtension = thumbDic["extension"] as? String else {
                print("[ERROR] - NO EVENT THUMBNAIL")
                return
        }
        //standard_small
        //landscape_small
        self.thumbSmall = thumbPath+"/landscape_small."+thumbExtension
        self.thumbLarge = thumbPath+"/landscape_large."+thumbExtension
        //print(thumbPath+"/standard_small."+thumbExtension)
    }
}
