//
//  FavoriteTableViewCell.swift
//  Marvel Challenge
//
//  Created by Ricardo Frealdo on 10/3/16.
//  Copyright © 2016 Frealdo. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {

    @IBOutlet weak var uiHeroName: UILabel?
    
    @IBAction func deleteAction(sender: AnyObject) {
        let myHeroDict : [String:String] = ["name": (self.uiHeroName?.text)!]
        NSNotificationCenter.defaultCenter().postNotificationName("sendHeroNameToDelete", object: self, userInfo: myHeroDict)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
